# File Renamer
File renamer is program for modifying names of multiple files. I have created this project to learn Java.

![screenshot](assets/file_renamer.png)

### Installation
1. Clone this repository
2. Run out/artifacts/File_renamer_2/File renamer 2.jar

License
----

MIT